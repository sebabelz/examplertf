#include <iostream>
#include <corehandsfree/hfc_public.h>
#include <coreincar/icc_public.h>

int main()
{
    double sumData[5] = {1, 2, 3, 4, 5};
    int dataLength = sizeof(sumData) / sizeof(double);
    std::cout << "(1 + 2 + 3 + 4 + 5) / 5 = " << mean(sumData, dataLength) << std::endl;;
//    std::cout << "(1 + 2 + 3 + 4 + 5) * 5 = " <<  << std::endl;;

    return 0;
}