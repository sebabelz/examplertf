#include <common/common_public.h>
#include "hfc_private.h"
double mean(double *data, int dataLength)
{
    return mean_priv(data, dataLength);
}

double mean_priv(double *data, int dataLength)
{
    double sum =  0;
    for (int i = 0; i < dataLength; ++i)
    {
        sum = add(sum, data[i]);
    }

    return divide(sum, (double)dataLength);
}
