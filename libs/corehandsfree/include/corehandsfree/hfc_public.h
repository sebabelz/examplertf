#ifndef HFC_PUBLIC_H
#define HFC_PUBLIC_H

#ifdef __cplusplus
extern "C"{
#endif

double mean(double *data, int dataLength);

#ifdef __cplusplus
}
#endif

#endif //HFC_PUBLIC_H