#ifndef COMMON_PRIVATE_H
#define COMMON_PRIVATE_H

double add_priv(const double first, const double second);
double subtract_priv(const double subtrahend, const double minuend);
double multiply_priv(const double multi, const double multiplier);
double divide_priv(const double num, const double denom);

#endif //COMMON_PRIVATE_H