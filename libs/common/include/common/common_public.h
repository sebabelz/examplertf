#ifndef COMMON_PUBLIC_H
#define COMMON_PUBLIC_H

#ifdef __cplusplus
extern "C"{
#endif

double add(const double first, const double second);
double subtract(const double subtrahend, const double minuend);
double multiply(const double multi, const double multiplier);
double divide(const double num, const double denom);

#ifdef __cplusplus
}
#endif


#endif //COMMON_PUBLIC_H