#include "common_private.h"

double add(const double first, const double second)
{
    return add_priv(first, second);
}

double subtract(const double subtrahend, const double minuend)
{
    return  subtract_priv(subtrahend, minuend);
}

double multiply(const double multi, const double multiplier)
{
    return multiply_priv(multi, multiplier);
}

double divide(const double num, const double denom)
{
    return divide_priv(num, denom);
}

double add_priv(const double first, const double second)
{
    return first + second;
}

double subtract_priv(const double subtrahend, const double minuend)
{
    return subtrahend - minuend;
}

double multiply_priv(const double multi, const double multiplier)
{
    return multi * multiplier;
}

double divide_priv(const double num, const double denom)
{
    return num / denom;
}